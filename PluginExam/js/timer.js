var date_actuelle = new Date();
var date_evenement = new Date("Feb 09 20:30:00 2024");
var total_secondes = (date_evenement - date_actuelle) / 1000;

var prefixe = "Vente terminée dans ";
if (total_secondes < 0) {
    prefixe = "Vente terminée il y a "; // On modifie le préfixe si la différence est négatif
    total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue
}

if (total_secondes > 0) {
    // A faire, tous nos calculs
}
else // Si total_secondes == 0 (puisque l'on a prit sa valeur absolue)
{
    compte_a_rebours.innerHTML = 'Vente terminée.';
}

var jours = Math.floor(total_secondes / (60 * 60 * 24));
var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));

minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));


compte_a_rebours.innerHTML = prefixe + jours + ' jours ' + heures + ' heures ' + minutes + ' minutes et ' + secondes + ' secondes.';

actualisation = setTimeout("compte_a_rebours();", 1000);