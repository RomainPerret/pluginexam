<?php

/**
 * Plugin Name: Plugin Exam
 * Description: Un super Plugin pour l'examen, spécialisé dans la vente de billets d'êvenements
 * Version: 1.0
 * Author: Romain
 * Licence: GPL2
 */

// Obligation d'ouvrir via Wordpress pour pouvoir lire le fichier.
// Sécurité

if (!defined('ABSPATH')) {
    exit;
}

// Vérification de l'activation des Plugins
// Apply_filter = affichage sous forme tableau
// (hook 'active_plugins')
// get_option récupérer dans la BDD wp-options
// In Array = dans un tableau

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    echo ' Le Plugin Woocommerce est activé';
} else {
    echo 'Le Plugin Woocommerce est désactivé';
}

if (in_array('advanced-custom-fields/acf.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    echo 'Le Plugin ACF est activé';
} else {
    echo 'Le Plugin ACF est désactivé';
}

add_action('acf/init', 'my_acf_init');
function my_acf_init()
{
    if (function_exists('acf_add_local_field_group'))

        // Rajouter des champs ACF

        acf_add_local_field_group(
            array(
                'key' => 'group_1',
                'title' => 'Evenement Drag Race',
                'fields' => array(
                    array(
                        'key' => 'field_1',
                        'label' => 'Date de l\'évènement',
                        'name' => 'date',
                        'type' => 'date_picker',
                    ),
                    array(
                        'key' => 'field_2',
                        'label' => 'Heure de l\'évènement',
                        'name' => 'heure',
                        'type' => 'time_picker',
                    ),
                    array(
                        'key' => 'field_3',
                        'label' => 'Description de l\'évènement',
                        'name' => 'description',
                        'type' => 'text',
                    ),
                    array(
                        'key' => 'field_4',
                        'label' => 'Informations privées de l\'évènement',
                        'name' => 'informations',
                        'type' => 'text',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'product',
                        ),
                    ),
                ),
            )
        );
}

// Ajouter action sur init :

add_action('init', 'detail_init_shortcode');
function detail_init_shortcode()
{
    add_shortcode('detailEvenement', 'detaileventshortcode');
}

// get_field et affichage sur la page :
function detaileventshortcode()
{
    $affichageevent = '<h4>L\'évènement a lieu le <strong>' . get_field('date') . '</strong></h4>
    <h4>L\'heure de début du show  : <strong>' . get_field('heure') . '</strong></h4>
    <h4>Description : <strong>' . get_field('description') . '</strong></h4>
    <h4>Infos supplémentaires : <strong>' . get_field('informations') . '</strong></h4>
    <div id="compte_a_rebours"> </div>';

    return $affichageevent;
}

// Hook pour ajouter une fiche de style

add_action('wp_enqueue_scripts', 'Plugin_FrontBack_enqueue_styles');

function Plugin_FrontBack_enqueue_styles()
{
    wp_enqueue_style('style', plugins_url('css/style.css', __FILE__));
}

// BONUS TIMER
// Ajout d'un fichier script avec un timer

add_action('wp_enqueue_scripts', 'Plugin_FrontBack_enqueue_scripts');
function Plugin_FrontBack_enqueue_scripts()
{
    wp_enqueue_script('script', plugins_url('js/timer.js', __FILE__), array('jquery'), '', true);
}








